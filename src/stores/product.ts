/* eslint-disable @typescript-eslint/no-unused-vars */
import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Product from "@/types/Product";
import productService from "@/services/product";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useProductStore = defineStore("product", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const products = ref<Product[]>([]);
  const dialog = ref(false);
  const deleteDialog = ref(false);
  const editedProduct = ref<Product>({ name: "", price: 0 });
  const deleteId = ref(0);

  watch(dialog, (newDialogValue, oldDialogValue) => {
    if (!newDialogValue) {
      clearEditedProduct();
    }
  });

  async function getProducts() {
    loadingStore.isLoading = true;
    try {
      const res = await productService.getProduct();
      products.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("Unable to get products");
    }
    loadingStore.isLoading = false;
  }

  async function saveProduct() {
    try {
      editedProduct.value.price = parseInt(
        editedProduct.value.price.toString()
      );
      if (editedProduct.value.id) {
        const res = await productService.updateProduct(
          editedProduct.value.id,
          editedProduct.value
        );
      } else {
        const res = await productService.saveProduct(editedProduct.value);
      }
      dialog.value = false;
      await getProducts();
    } catch (e) {
      console.log(e);
      messageStore.showError("Unable to save product");
    }
  }

  function updateProduct(product: Product) {
    editedProduct.value = JSON.parse(JSON.stringify(product));
    dialog.value = true;
  }

  async function deleteProduct() {
    try {
      const res = await productService.deleteProduct(deleteId.value);
      deleteDialog.value = false;
      deleteId.value = 0;
      await getProducts();
    } catch (e) {
      console.log(e);
      messageStore.showError("Unable to delete product");
    }
  }

  function clearEditedProduct() {
    editedProduct.value = { name: "", price: 0 };
  }

  function deleteConfirm(id: number) {
    deleteDialog.value = true;
    deleteId.value = id;
  }

  return {
    products,
    getProducts,
    dialog,
    deleteDialog,
    editedProduct,
    saveProduct,
    updateProduct,
    deleteProduct,
    deleteConfirm,
  };
});
